# First demo, a simple MCMC code of a particle in a double well potential

import numpy as np
from numpy.random import rand

T = 1000 # K
k = 8.6173303e-5 # Boltzman constant eV K^-1
beta = 1 / (T*k)
N = 10000 # Number of walkers

def V(x):
    return x**4-x**2

x = np.zeros((N,)) # Initialize positions

d = 1.0

E = V(x)
iter = 0
maxiter = 10000
equilibration = 1000

bins = np.arange(-2, 2, 0.01)
dist = 0

ref_dist = np.exp(-V(bins+0.01/2)*beta)
ref_dist /= np.sum(ref_dist)
while 1:
    E = V(x)
    xp = x + (rand(N)-0.5)*d
    Ep = V(xp)
    dE = Ep - E
    p = rand(N) # Acceptance probabilities
    S = np.logical_or(dE < 0, p<np.exp(-dE*beta))
    print "Iter: ", iter, "Acceptance", sum(S) * 1.0 / N
    x = np.where(S, xp, x)
    iter += 1
    if iter > maxiter:
        break
    if iter > equilibration:
        hist, bin_edges = np.histogram(x, bins=bins)
        dist += hist
        if iter % 1000 == 0 and iter > 1500:
            norm = 1.0 / np.sum(dist)
            f = open('dist%02d.txt' % (iter // 1000),'w')
            for b1,b2 in zip(ref_dist, dist):
                print >>f, b1,b2 * norm
            f.close()
